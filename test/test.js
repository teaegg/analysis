var analysis = require("../index.js");

var result;

result = analysis.load('/home/gk/Projects/node/analysis/test/vectors.bin');
console.log(JSON.stringify(result));

result = analysis.relativeWords('智能', 10);
console.log(JSON.stringify(result));

result = analysis.relativeWords(['智能', '手机'], 10);
console.log(JSON.stringify(result));

result = analysis.analogy('中国', '北京', '法国', 10);
console.log(JSON.stringify(result));

result = analysis.compare('广东', '广西');
console.log(JSON.stringify(result));

result = analysis.compare('广东', '湖南');
console.log(JSON.stringify(result));

result = analysis.compare(['房间', '出租'], ['出售', '房屋']);
console.log(JSON.stringify(result));

result = analysis.compare(['房间', '出租'], ['出售']);
console.log(JSON.stringify(result));

result = analysis.compare(['房间', '出租'], ['房屋']);
console.log(JSON.stringify(result));