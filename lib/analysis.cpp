#include "analysis.h"
#include "word2vec.cpp"

Word2Vec* gW2c;

NAN_METHOD(load) {
  NanScope();

  if(args.Length() != 1)
    NanReturnValue (NanNew<v8::Integer>(-1));

  String::Utf8Value binPath(args[0]->ToString());

  gW2c = new Word2Vec();
  int ret = gW2c->Load(*binPath);

  NanReturnValue (NanNew<v8::Integer>(ret));
}

NAN_METHOD(relativeWords) {
  NanScope();

  if(args.Length() != 2)
    NanReturnValue (NanNew<v8::Integer>(-1));

  uint64_t num = args[1]->NumberValue();
	vector<Word2Vec::WordSim> wordSim;
  if(args[0]->IsArray()) {
	  Local<Array> words = Local<Array>::Cast(args[0]);
	  vector<string> wordsStr;
	  for(int i = 0; i < words->Length(); ++i)
	  	wordsStr.push_back(*String::Utf8Value(words->Get(i)));
	  int ret = gW2c->Distance(wordsStr, num, wordSim);
  } else {
	  String::Utf8Value word(args[0]->ToString());
	  int ret = gW2c->Distance(*word, num, wordSim);
	}
	Local<Array> outArray;
	WrapWordVector(wordSim, outArray);
	NanReturnValue(outArray);
}

NAN_METHOD(analogy) {
  NanScope();

  if(args.Length() != 4)
    NanReturnValue (NanNew<v8::Integer>(-1));

  String::Utf8Value sa(args[0]->ToString());
  String::Utf8Value sb(args[1]->ToString());
  String::Utf8Value sc(args[2]->ToString());
  uint64_t num = args[3]->NumberValue();
  vector<Word2Vec::WordSim> wordSim;

  int ret = gW2c->Analogy(*sa, *sb, *sc, num, wordSim);

  Local<Array> outArray;
  WrapWordVector(wordSim, outArray);
  NanReturnValue(outArray);
}

NAN_METHOD(compare) {
  NanScope();

  if(args.Length() != 2)
    NanReturnValue (NanNew<v8::Integer>(-1));

  float score = -1;
  if(args[0]->IsArray() && args[1]->IsArray()) {
    vector<string> wa;
    vector<string> wb;
    Local<Array> words = Local<Array>::Cast(args[0]);
    for(int i = 0; i < words->Length(); ++i)
      wa.push_back(*String::Utf8Value(words->Get(i)));

    words = Local<Array>::Cast(args[1]);
    for(int i = 0; i < words->Length(); ++i)
      wb.push_back(*String::Utf8Value(words->Get(i)));
    int ret = gW2c->CosineDistance(wa, wb, score);
  } else {
    String::Utf8Value sa(args[0]->ToString());
    String::Utf8Value sb(args[1]->ToString());
    int ret = gW2c->CosineDistance(*sa, *sb, score);
  }

  NanReturnValue(score);
}
