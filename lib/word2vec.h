/*
 * word2vec.h
 *
 */
 
#ifndef WORD2VEC_H_
#define WORD2VEC_H_
 
#include <stdint.h>
#include <vector>
#include <string>
#include <unordered_map>

class Word2Vec {
public:
    Word2Vec();
    virtual ~Word2Vec();
    int Load(const char *path);
    int Release();
    int CosineDistance(const std::vector<std::string> &a,
            const std::vector<std::string> &b, float &score);
    int CosineDistance(const std::string &a, const std::string &b, float &score);
    struct WordSim {
        std::string word;
        float sim;
        bool operator > (const WordSim &other) const {
            return sim > other.sim;
        }
    };
    /**
     * @brief 给定一个词，找出最相似的n个词
     */
    int Distance(const std::string &word, const uint64_t &max_size,
            std::vector<WordSim> &sim_words);
 
    int Distance(const std::vector<std::string> &words, const uint64_t &max_size,
            std::vector<WordSim> &sim_words);
    /**
     * @brief b - a + c，找出最相近的n个词
     */
    int Analogy(const std::string &a, const std::string &b, const std::string &c,
            const uint64_t &max_size, std::vector<WordSim> &sim_words);
private:
    const float *GetVec(const std::string &word);
    int Normalize(float *vec);
    int Sim(const float *va, const float *vb, float &sim);
private:
    float *matrix;
    std::vector<std::string> *vocab;
    std::unordered_map<uint64_t, uint64_t> *map;
    uint64_t word_num;
    uint64_t vec_size;
};

#endif