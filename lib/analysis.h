#ifndef ANALYSIS_SRC_ANALYSIS_H
#define ANALYSIS_SRC_ANALYSIS_H

#include "utils.h"

extern NAN_METHOD(load);
extern NAN_METHOD(relativeWords);
extern NAN_METHOD(analogy);
extern NAN_METHOD(compare);

#endif // ANALYSIS_SRC_ANALYSIS_H
