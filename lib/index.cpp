#include "analysis.h"


void init(Handle<Object> exports) {
  exports->Set(NanNew("load"),
        NanNew<FunctionTemplate>(load)->GetFunction());
  exports->Set(NanNew("relativeWords"),
        NanNew<FunctionTemplate>(relativeWords)->GetFunction());
  exports->Set(NanNew("analogy"),
        NanNew<FunctionTemplate>(analogy)->GetFunction());
  exports->Set(NanNew("compare"),
        NanNew<FunctionTemplate>(compare)->GetFunction());
}

NODE_MODULE(w2c, init)
