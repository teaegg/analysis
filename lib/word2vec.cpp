/*
 * word2vec.cpp
 *
 */
 
#include "word2vec.h"
#include <stdio.h>
#include <math.h>
#include <new>
#include <queue>
#include <unordered_set>
 
using namespace std;

const uint64_t BUF_SIZE = 1024;
 
Word2Vec::Word2Vec() : matrix(NULL), vocab(NULL), map(NULL), word_num(0), vec_size(0) {
 
}
 
Word2Vec::~Word2Vec() {
    Release();
}
 
int Word2Vec::Release() {
    delete []matrix;
    delete vocab;
    delete map;
 
    matrix = NULL;
    vocab = NULL;
    map = NULL;
    return 0;
}
 
const float *Word2Vec::GetVec(const string &word) {
    std::hash<std::string> hash_fn;
    uint64_t sign = hash_fn(word);
    const uint64_t *value;
    // int ret = map->Get(sign, value);
    // if (ret != 0) {
    //     fprintf(stderr, "word [%s] not in model\n", word.c_str());
    //     return NULL;
    // }
    unordered_map<uint64_t, uint64_t>::const_iterator got = map->find(sign);
    if (got == map->end())
        return NULL;
    else
        value = &(got->second);
    return &matrix[(*value) * vec_size];
}
 
int Word2Vec::CosineDistance(const string &a, const string &b, float &score) {
    const float *va;
    const float *vb;
    va = GetVec(a);
    vb = GetVec(b);
    if (va == NULL || vb == NULL) {
        return -1;
    }
    Sim(va, vb, score);
    return 0;
}
 
int Word2Vec::CosineDistance(const vector<string> &a, const vector<string> &b,
        float &score) {
    if (a.empty() || b.empty()) {
        fprintf(stderr, "empty param\n");
        return -1;
    }
 
    float va[vec_size];
    float vb[vec_size];
    memset(&va, 0, vec_size * sizeof(float));
    memset(&vb, 0, vec_size * sizeof(float));
    uint64_t sign;
 
    int ret;
    const float *vec;
 
    for (vector<string>::const_iterator it = a.begin(); it != a.end(); ++it) {
        vec = GetVec(*it);
        if (vec == NULL) {
            return -1;
        }
 
        for (int i = 0; i < vec_size; ++i) {
            va[i] += vec[i];
        }
    }
 
    for (vector<string>::const_iterator it = b.begin(); it != b.end(); ++it) {
        vec = GetVec(*it);
        if (vec == NULL) {
            return -1;
        }
 
        for (int i = 0; i < vec_size; ++i) {
            vb[i] += vec[i];
        }
    }
 
    ret = Normalize(va);
    if (ret == -1) {
        fprintf(stderr, "nomaize va fail\n");
        return -1;
    }
 
    ret = Normalize(vb);
    if (ret == -1) {
        fprintf(stderr, "nomaize vb fail\n");
        return -1;
    }
 
    Sim(va, vb, score);
    return 0;
}
 
int Word2Vec::Normalize(float *vec) {
    if (vec == NULL) {
        return -1;
    }
    float len = 0;
    for (int i = 0; i < vec_size; ++i) {
        len += vec[i] * vec[i];
    }
    len = sqrt(len);
    for (int i = 0; i < vec_size; ++i) {
        vec[i] /= len;
    }
    return 0;
}
 
int Word2Vec::Load(const char *path) {
    Release();
    char ch;
    char buf[BUF_SIZE];
    int ret = -1;
    uint64_t sign;
    // uint64_t map_size;
 
    FILE *f = fopen(path, "rb");
    if (f == NULL) {
        fprintf(stderr, "open word2vec file [%s] fail\n", path);
        goto LOAD_END;
    }
 
    fscanf(f, "%lld", &word_num);
    fscanf(f, "%lld", &vec_size);
    fprintf(stderr, "word_num: %lld, vec_size: %lld\n", word_num, vec_size);
    matrix = new (nothrow) float[word_num * vec_size];
    if (matrix == NULL) {
        fprintf(stderr, "alloc mem for matrix fail");
        ret = -2;
        goto LOAD_END;
    }
 
    vocab = new (nothrow) vector<string>();
    if (vocab == NULL) {
        fprintf(stderr, "alloc mem for vocab fail");
        ret = -3;
        goto LOAD_END;
    }
    vocab->reserve(word_num);
 
    map = new (nothrow) unordered_map<uint64_t, uint64_t>();
    if (map == NULL) {
        fprintf(stderr, "alloc mem for map fail");
        ret = -4;
        goto LOAD_END;
    }
 
    // map_size = find_min_prime(word_num * 2);
    // ret = map->Create(map_size);
    // if (ret == -1) {
    //     fprintf(stderr, "create map fail");
    //     goto LOAD_END;
    // }

    std::hash<std::string> hash_fn;
    for (uint64_t i = 0; i < word_num; ++i) {
        fscanf(f, "%s%c", &buf, &ch);
        buf[BUF_SIZE - 1] = '\0';
        vocab->push_back(buf);
        for (uint64_t j = 0; j < vec_size; ++j) {
            fread(&matrix[j + i * vec_size], sizeof(float), 1, f);
        }
        Normalize(&matrix[i * vec_size]);
        sign = hash_fn(vocab->back());
        (*map)[sign] = i;
    }
    return ret;
LOAD_END:
    Release();
    return ret;
}
 
int Word2Vec::Analogy(const string &a, const string &b, const string &c,
        const uint64_t &max_size, vector<WordSim> &sim_words) {
    sim_words.clear();
    if (matrix == NULL || vocab == NULL || map == NULL) {
        return -1;
    }
    priority_queue<WordSim, vector<WordSim>, greater<WordSim> >sim_queue;
    std::hash<std::string> hash_fn;
    uint64_t sa = hash_fn(a);
    uint64_t sb = hash_fn(b);
    uint64_t sc = hash_fn(c);
 
    // const uint64_t *value;
    // int ret = map->Get(sa, value);
    // if (ret != 0) {
    //     return -1;
    // }
    // uint64_t pa = *value;

    uint64_t pa;
    uint64_t pb;
    uint64_t pc;
    unordered_map<uint64_t, uint64_t>::const_iterator got = map->find(sa);
    if (got == map->end())
        return -1;
    else
        pa = got->second;
 
    got = map->find(sb);
    if (got == map->end())
        return -1;
    else
        pa = got->second;

    got = map->find(sc);
    if (got == map->end())
        return -1;
    else
        pa = got->second;
    // ret = map->Get(sb, value);
    // if (ret != 0) {
    //     return -1;
    // }
    // uint64_t pb = *value;
 
    // ret = map->Get(sc, value);
    // if (ret != 0) {
    //     return -1;
    // }
    // uint64_t pc = *value;
 
    float *va = &matrix[pa * vec_size];
    float *vb = &matrix[pb * vec_size];
    float *vc = &matrix[pc * vec_size];
 
    float vec[vec_size];
    for (int i = 0; i < vec_size; ++i) {
        vec[i] = vb[i] - va[i] + vc[i];
    }
 
    Normalize(vec);
    WordSim word_sim;
    for (uint64_t i = 0; i < word_num; ++i) {
        if (i == pa || i == pb || i == pc) {
            continue;
        }
 
        float *cur_vec = &matrix[i * vec_size];
        Sim(vec, cur_vec, word_sim.sim);
        if (sim_queue.size() >= max_size && word_sim.sim <= sim_queue.top().sim) {
            continue;
        }
        word_sim.word = vocab->operator [](i);
        sim_queue.push(word_sim);
        if (sim_queue.size() > max_size) {
            sim_queue.pop();
        }
    }
    sim_words.resize(sim_queue.size());
    size_t i = sim_words.size();
    while (! sim_queue.empty()) {
        sim_words[--i] = sim_queue.top();
        sim_queue.pop();
    }
    return 0;
}
 
int Word2Vec::Distance(const vector<string> &words, const uint64_t &max_size,
        vector<WordSim> &sim_words) {
    sim_words.clear();
    if(matrix == NULL)
        return -2;
    if(vocab == NULL)
        return -3;
    if(map == NULL)
        return -4;
    // if (matrix == NULL || vocab == NULL || map == NULL) {
    //     return -1;
    // }
 
    float v[vec_size];
    memset(&v, 0, vec_size * sizeof(float));
    const float *vec;
    const uint64_t *value;
    int ret;
    unordered_set<uint64_t> pos_set;
    std::hash<std::string> hash_fn;
    for (vector<string>::const_iterator it = words.begin(); it != words.end(); ++it) {
        uint64_t sign = hash_fn(it->c_str());
        // ret = map->Get(sign, value);
        // if (ret != 0) {
        //     return -1;
        // }
        unordered_map<uint64_t, uint64_t>::const_iterator got = map->find(sign);
        if (got == map->end())
            return NULL;
        else
            value = &(got->second);

        vec = &matrix[*value * vec_size];
 
        for (int i = 0; i < vec_size; ++i) {
            v[i] += vec[i];
        }
        pos_set.insert(*value);
    }
 
    Normalize(v);
    priority_queue<WordSim, vector<WordSim>, greater<WordSim> >sim_queue;
    WordSim word_sim;
    for (uint64_t i = 0; i < word_num; ++i) {
        if (pos_set.find(i) != pos_set.end()) {
            continue;
        }
        float *cur_vec = &matrix[i * vec_size];
        Sim(v, cur_vec, word_sim.sim);
        if (sim_queue.size() >= max_size && word_sim.sim <= sim_queue.top().sim) {
            continue;
        }
        word_sim.word = vocab->operator [](i);
        sim_queue.push(word_sim);
        if (sim_queue.size() > max_size) {
            sim_queue.pop();
        }
    }
    sim_words.resize(sim_queue.size());
    size_t i = sim_words.size();
    while (! sim_queue.empty()) {
        sim_words[--i] = sim_queue.top();
        sim_queue.pop();
    }
    return 0;
}
 
int Word2Vec::Distance(const string &word, const uint64_t &max_size,
        vector<WordSim> &sim_words) {
    sim_words.clear();
    if(matrix == NULL)
        return -2;
    if(vocab == NULL)
        return -3;
    if(map == NULL)
        return -4;
    // if (matrix == NULL || vocab == NULL || map == NULL) {
    //     return -1;
    // }
 
    priority_queue<WordSim, vector<WordSim>, greater<WordSim> >sim_queue;
    std::hash<std::string> hash_fn;
    uint64_t word_sign = hash_fn(word);
    // int ret = map->Get(word_sign, value);
    // if (ret != 0) {
    //     return -1;
    // }
    uint64_t pos;
    unordered_map<uint64_t, uint64_t>::const_iterator got = map->find(word_sign);
    if (got == map->end())
        return 0;
    else
        pos = got->second;
    WordSim word_sim;
    float *vec = &matrix[pos * vec_size];
    for (uint64_t i = 0; i < word_num; ++i) {
        if (i == pos) {
            continue;
        }
 
        float *cur_vec = &matrix[i * vec_size];
        Sim(vec, cur_vec, word_sim.sim);
        if (sim_queue.size() >= max_size && word_sim.sim <= sim_queue.top().sim) {
            continue;
        }
        word_sim.word = vocab->operator [](i);
        sim_queue.push(word_sim);
        if (sim_queue.size() > max_size) {
            sim_queue.pop();
        }
    }
    sim_words.resize(sim_queue.size());
    size_t i = sim_words.size();
    while (! sim_queue.empty()) {
        sim_words[--i] = sim_queue.top();
        sim_queue.pop();
    }
    return 0;
}
 
int Word2Vec::Sim(const float *va, const float *vb, float &sim) {
    if (va == NULL || vb == NULL) {
        return -1;
    }
    sim = 0;
    for (uint64_t i = 0; i < vec_size; ++i) {
        sim += va[i] * vb[i];
    }
    return 0;
}
